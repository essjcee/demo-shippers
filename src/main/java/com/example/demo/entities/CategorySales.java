package com.example.demo.entities;

public class CategorySales {
	private String category;
	private int sales;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getSales() {
		return sales;
	}
	public void setSales(int sales) {
		this.sales = sales;
	}
	public CategorySales(String category, int sales) {
		super();
		this.category = category;
		this.sales = sales;
	}
	public CategorySales() {
		
	}
}
